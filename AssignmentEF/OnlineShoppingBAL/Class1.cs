﻿using OnlineShoppingBusinessObject;
using OnlineShoppingDAL;

namespace OnlineShoppingBAL
{
    public class Class1
    {
        dal dald = new dal();
        public int AddUser(User user)
        {
            return dald.AddUser(user);



        }



        public Role GetRole(int roleid)
        {
            return dald.GetRole(roleid);



        }



        public int DeleteUser(int userid)
        {
            return dald.DeleteUser(userid);
        }

        public int AddProduct(Product product)
        {
            return dald.AddProduct(product);    
        }
        public int DeleteProduct(int productId)
        {
            return dald.DeleteProduct(productId);
        }
        public List<Product> DisplayProducts() 
        {
            return dald.DisplayProducts();
        }
        public User GetUser(string username)
        {
            return dald.GetUser(username);
        }
        public Product GetProduct(String productName) 
        {
            return dald.GetProduct(productName);
        }
        public int AddOrder(Order order)
        {
            return dald.AddOrder(order);
        }
        public int UpdateProduct(string name,Product product)
        {
            return dald.UpdateProduct(name, product);
        }

    }
}
