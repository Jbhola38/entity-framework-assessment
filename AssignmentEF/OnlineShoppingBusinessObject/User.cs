﻿namespace OnlineShoppingBusinessObject
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public DateTime? DateCreated {  get; set; }
        public DateTime? DateUpdated { get; set; }
        public bool IsActive { get; set; }

        public Role Role { get; set; }
    }
}