﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShoppingBusinessObject
{
    public class Order
    {
        public int OrderId { get; set; }
        public Product Product { get; set; }    
        public User User { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }


    }
}
