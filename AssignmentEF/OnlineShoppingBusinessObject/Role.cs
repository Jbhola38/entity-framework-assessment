﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShoppingBusinessObject
{
    public class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }   
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }   

    }
}
