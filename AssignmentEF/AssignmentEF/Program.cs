﻿using OnlineShoppingBusinessObject;
using System.Reflection.Metadata;

namespace AssignmentEF
{
    internal class Program
    {
        static void Main(string[] args)
        {
            OnlineShoppingBAL.Class1 bal = new OnlineShoppingBAL.Class1 ();
            Console.WriteLine("MAIN MENU");
            Console.WriteLine("Choose Your Role\n1.Admin\n2.Supplier\n3.Customer");


            int ch= byte.Parse(Console.ReadLine());
            switch(ch) {
                case 1:
                    {
                        Console.WriteLine("Who Do you want to do\n1.Add user\n2.Delete user");
                        int cho = byte.Parse(Console.ReadLine());
                        Console.WriteLine("press\n2.Supplier\n3.Customer");
                        int choi= byte.Parse(Console.ReadLine());
                        Role role = bal.GetRole(choi);
                        switch(cho)
                        {
                            case 1:
                                {
                                    Console.WriteLine("Enter The Name:");
                                   string name = Console.ReadLine();
                                    User user = new User()
                                    {
                                        UserName = name,
                                        DateCreated = DateTime.Now,
                                        DateUpdated = null,
                                        IsActive = true,
                                        Role = role

                                    };
                                    bal.AddUser(user);
                                    
                                    break;
                                }
                            case 2:
                                {
                                    Console.WriteLine("Enter the user Id");
                                    int userId=byte.Parse(Console.ReadLine());
                                    bal.DeleteUser(userId);
                                    break;
                                }
                            
                            default:
                                {
                                    Console.WriteLine("Enter a Valid Input");
                                    break;
                                }
                        }
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Chosse The Options");
                        Console.WriteLine("1.Add Products\n2.Delete Protucts\n3.Edit Products");
                        int cho=byte.Parse(Console.ReadLine()); 
                        switch(cho)
                        {
                            case 1:
                                {
                                    Console.WriteLine("Enter The Product Name");
                                    string name = Console.ReadLine();
                                    Console.WriteLine("Enter The Product Price");
                                    int productPrice= Convert.ToInt32(Console.ReadLine());
                                    Product product = new Product()
                                    {
                                        ProductName = name,
                                        ProductPrice = productPrice,
                                        DateCreated = DateTime.Now,
                                        DateUpdated = null,
                                        IsActive = true,
                                    };
                                    bal.AddProduct(product);
                                    break;
                                }
                            case 2:
                                {
                                    Console.WriteLine("Enter The Product Id");
                                    int id=byte.Parse(Console.ReadLine());
                                    bal.DeleteProduct(id);
                                    break;
                                }
                            case 3:
                                {
                                    Console.WriteLine("Enter The Name Of the Product to edit");
                                    string name= Console.ReadLine();
                                    Product product = bal.GetProduct(name);
                                    if(product!=null)
                                    {
                                        Console.WriteLine("Enter The new Name");
                                        string? ProdutName=Console.ReadLine();
                                        Console.WriteLine("Enter The new Price");
                                        int Price; 
                                        Int32.TryParse(Console.ReadLine(), out Price);
                                        Product updatedProduct = new Product()
                                        {
                                            ProductName = ProdutName,
                                            ProductPrice = Price,
                                        };
                                        bal.UpdateProduct(name, updatedProduct);
                                    }
                                    break;
                                }
                            default:
                                {
                                    Console.WriteLine("Enter A valid Input");
                                    break;
                                }
                        }
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Choose The Option");
                        Console.WriteLine("1.See All the Products\n2.Place order");
                        int choic=byte.Parse(Console.ReadLine());
                        switch (choic)
                        {
                            case 1:
                                {
                                    List<Product> list = new List<Product>();
                                   
                                    list = bal.DisplayProducts();  
                                    foreach(Product product in list)
                                    {
                                        Console.WriteLine($"{product.ProductId}--{product.ProductName}--{product.ProductPrice}");
                                    }
                                    break;
                                }
                                case 2:
                                {
                                    List<Product> list = new List<Product>();

                                    list = bal.DisplayProducts();
                                    foreach (Product product in list)
                                    {
                                        Console.WriteLine($"{product.ProductId}--{product.ProductName}--{product.ProductPrice}");
                                    }
                                    Console.WriteLine("Choose product Name to place Order");
                                    string productName=Console.ReadLine();
                                    Console.WriteLine("Enter User Name");
                                    string userName=Console.ReadLine();
                                    Product p= bal.GetProduct(productName);
                                    User user= bal.GetUser(userName);
                                    Order order = new Order()
                                    {
                                        DateCreated = DateTime.Now,
                                        DateUpdated=null,
                                        Product=p,
                                        User=user,

                                    };
                                    bal.AddOrder(order);
                                    Console.WriteLine("Order Has Been placed");



                                    break;
                                }
                            default:
                                Console.WriteLine("Enter A valid Input");
                                break;
                        }
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Enter A valid Input");
                        break;
                    }
            }
        }
    }
}