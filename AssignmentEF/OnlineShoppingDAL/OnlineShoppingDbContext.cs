﻿using Microsoft.EntityFrameworkCore;
using OnlineShoppingBusinessObject;

namespace OnlineShoppingDAL
{
    public class OnlineShoppingDbContext : DbContext
    {
        public OnlineShoppingDbContext()
        {

        }
        public OnlineShoppingDbContext(DbContextOptions<OnlineShoppingDbContext> options) : base(options) { }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }    

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("data source=INW-936\\SQLEXPRESS;initial catalog=EcommerceDb;integrated security=true;TrustServerCertificate=true");
        }
    }
}