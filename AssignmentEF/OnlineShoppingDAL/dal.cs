﻿using OnlineShoppingBusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShoppingDAL
{
    public class dal
    {
        OnlineShoppingDbContext db= new OnlineShoppingDbContext();
        public int AddUser(User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
            return 1;
        }



        public int DeleteUser(int userid)
        {
            User user = db.Users.Find(userid);
            if (user != null)
            {
                user.IsActive = false;
                db.SaveChanges();
                return 1;
            }
            return 0;
        }
        public Role GetRole(int id)
        {
            Role role = db.Roles.Find(id);
            return role;
        }
        
        public int AddProduct(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges(); 
            return 1;
        }
        public int DeleteProduct(int id)
        {
            Product product = db.Products.Find(id);
            if(product!=null)
            {
                product.IsActive = false; 
                db.SaveChanges();
                return 1;
            }
            return 0;
        }
        
        public List<Product> DisplayProducts()
        {
          return db.Products.ToList();
        }

        
        public User GetUser(string userName)
        {
            User user = db.Users.FirstOrDefault(x => x.UserName == userName);
            return user;
        }
        public Product GetProduct(string userName) 
        {
            Product product=db.Products.FirstOrDefault(x=>x.ProductName == userName);
            return product;
        }
        public int AddOrder(Order order)
        {
            db.Orders.Add(order);
            db.SaveChanges();
            return 1;
        }
        public int UpdateProduct(String productName, Product product)
        {
            Product obj = db.Products.Where(x => x.ProductName == productName).FirstOrDefault();
            if (obj != null)
            {
                if(product.ProductName.Length>0)
                    obj.ProductName=product.ProductName;
                if(product.ProductPrice>0)
                    obj.ProductPrice=product.ProductPrice;
                db.Products.Update(obj);
                db.SaveChanges();
            }
            else
            {
                return 0;
            }
            return 1;
        }
    }
}
